// ==UserScript==
// @name           paste-enable
// @namespace      http://d.hatena.ne.jp/Cherenkov/
// @include        http*
// ==/UserScript==

// 入れ替えるイベントリスナ
var f = function(){ return true; };

// 書き換え対象のDOMイベント名
var evs = "beforecopy beforecut click contextmenu copy dragstart mousedown mouseup selectstart".split(" ");

// 書き換え対象のCSS属性名
var props = "userSelect MozUserSelect MsUserSelect WebkitUserSelect".split(" ");

// １要素に対する書き換え操作
function replace_events( elem ){
  // イベント
  for( var i = 0; i < evs.length; i ++ ){
    elem[ "on" + evs[i] ] = f();
  }
  // CSS属性
  if(elem.style){
    for( var i = 0; i < props.length; i ++ ){
       elem.style[  props[i] ] = "";
    }
  }
}

// 全要素に対し
var elems = document.getElementsByTagName("*");
for(var i = 0; i < elems.length; i ++){
  replace_events( elems[i] );
}
replace_events( document );
