﻿// ==UserScript==
// @name            Nexusmods skyrim japan database
// @namespace       mikanNsjd
// @version         1.0
// @description     Nexusmodsのスカイリムページに、日本MODデータベースへのリンクを追加します
// @author          mikan-megane
// @require         https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js
// @include         http://www.nexusmods.com/skyrim/mods/*
// @include         http://nexusmods.com/skyrim/mods/*
// ==/UserScript==

var id = document.URL.replace(/http:\/\/(www\.)?nexusmods\.com\/skyrim\/mods\/(\d+)($|\/.+)/, "$2");

jQuery.noConflict();
(function($){
    $(function(){
		$(".tabbed-buttons div:first-child").before('<div class="action-button green"><a target="_blank" href="http://skyrim.2game.info/detail.php?id='+id+'">日本MODデータベースへ</a></div> ');
    });
})(jQuery);