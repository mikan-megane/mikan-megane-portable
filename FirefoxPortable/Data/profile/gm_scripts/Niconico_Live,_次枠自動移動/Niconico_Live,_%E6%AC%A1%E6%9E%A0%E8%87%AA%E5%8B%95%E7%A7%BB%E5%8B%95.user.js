﻿// ==UserScript==
// @name        Niconico Live, 次枠自動移動
// @description ニコニコ生放送の参加コミュニティの次枠移動を可能にします。放送履歴が公開されているコミュニティのタイムシフト次枠移動も可能。
// @namespace   https://greasyfork.org/users/1242
// @include     http://live.nicovideo.jp/watch/*
// @include     http://com.nicovideo.jp/live_archives/*
// @version     1.1.2
// @grant       none
// @run-at      document-end
// ==/UserScript==
(function() {
    var window = typeof unsafeWindow == "undefined" ? window : unsafeWindow, document = window.document;
    var DEBUG = false;
    function log() {
        if (DEBUG) {
            var array = [];
            for (var i = 0; i < arguments.length; i++) {
                array.push(arguments[i]);
            }
            return console.log.apply(console, array);
        }
    }

    var loc = document.location;
    if (loc.host == "live.nicovideo.jp" && loc.pathname.indexOf("/watch/") != -1) {
        var $ = window.jQuery;
        if (!$ && window.jQueries) {
            for (var v in window.jQueries) {
                $ = window.jQueries[v];
            }
        }
        $(watchPageLoaded);
    } else if (loc.host == "com.nicovideo.jp" && loc.pathname.indexOf("/live_archives/") != -1) {
        window.Event.observe(window, "load", archivesPageLoaded);
    }
    
    function watchPageLoaded() {
        var startTime, communityId, liveId, isEnable, timer;
        function setObserve(toIsEnable) {
            if (toIsEnable) {
                isEnable = true;
                timer = clearTimeout(timer);
                timer = setTimeout(observe, 0);
            } else {
                isEnable = false;
                timer = clearTimeout(timer);
            }
            $("#auto_next").updateToggleText();
        }
        function parseCommunityId(src) {
            var m = src.match(/(ch|co)\d+/);
            return m ? m[0] : null;
        }
        function parseLiveId(url) {
            var m = url.match(/lv\d+/);
            return m ? m[0] : null;
        }
        function getStartTime() {
            var text = $("#description_tab_view .information").text();
            var m = text.match(/(\d+)\/(\d+)\/(\d+).*(\d+)：(\d+)開演/);
            for (var i = 1; i < m.length; i++) {
                m[i] = parseInt(m[i], 10);
            }
            return m ? new Date(m[1], m[2] - 1, m[3], m[4], m[5], 0, 0) : null;
        }
        function getCommunityId() {
            // http://icon.nimg.jp/community/s/***/co***.jpg?***
            // http://icon.nimg.jp/channel/s/ch***.jpg?***
            return parseCommunityId($("#watch_title_box .image, #gate .com .bd").attr("src") || "");
        }
        function getTimeout() {
            var diff = new Date().getTime() - startTime.getTime();
            var min = Math.round(diff / 60000);
            if (min <= 28) {
                return 30 * 1000;
            } else if (min <= 32) {
                return 6 * 1000;
            } else if (min <= 35) {
                return 10 * 1000;
            } else if (min <= 597) {
                return 30 * 1000;
            } else if (min <= 602) {
                return 10 * 1000;
            } else {
                return 30 * 1000;
            }
        }
        function eachNotifybox(callback) {
            if (!callback) { return; }
            for (var page = 1; ; page++) {
                // Notifyboxを読み込む（同期的に行われる）
                if (window.Nicolive.Notifybox.reload_stream_list(page, 5) !== false) {
                    $("#notify_box_container .programList li").each(function (i, e) {
                        var co = parseCommunityId($(".thmb", e).attr("src"));
                        var lv = parseLiveId($("a", e).attr("href"));
                        callback.call(e, co, lv);
                    });
                }
                var $next = $("#notify_box_container .arrowRight");
                if ($next.length == 0) {
                    break;
                }
            }
        }
        function observe() {
            if (!isEnable) return;
            log("observe()");

            try {
                // 現在の放送の情報を取得
                startTime = getStartTime();
                communityId = getCommunityId();
                liveId = window.VideoId;
                log("videoInfo", startTime, communityId, liveId);

                if (!startTime || !communityId || !liveId) {
                    return;
                }
                var liveIdInt = parseInt(liveId.substr(2), 10);

                eachNotifybox(function (co, lv) {
                    if (co && lv) {
                        if (communityId == co && liveIdInt < parseInt(lv.substr(2), 10)) {
                            window.location.href = "http://live.nicovideo.jp/watch/" + lv + "?auto";
                        }
                    }
                });
            } catch (ex) {
                log("observe error!");
                log(ex);
            }

            timer = setTimeout(observe, getTimeout());
        }
        $.fn.addToUtilityMenu = function () {
            return this.each(function () {
                $("<li />")
                    .append(this)
                    .appendTo("#watch_player_bottom_box .utilty_menu");
            });
        }
        $.fn.updateToggleText = function () {
            if (isEnable) {
                $(this).css({
                    backgroundColor: "rgba(255,255,0,0.3)",
                    fontWeight: "bold",
                    textShadow: "0 0 0"
                }).text("次枠移動を無効にする");
            } else {
                $(this).css({
                    backgroundColor: "",
                    fontWeight: "",
                    textShadow: ""
                }).text("次枠移動を有効にする");
            }
            return this;
        }
        
        $("<a />")
            .attr({
                id: "auto_next",
                href: "javascript:void(0)"
            })
            .updateToggleText()
            .click(function () {
                setObserve(!isEnable);
            })
            .addToUtilityMenu();
        $('<a />')
            .attr({
                id: "next_ts",
                href: "javascript:void(0)",
            })
            .text("次のタイムシフトを探す")
            .click(function () {
                $(this).text("次のタイムシフトを探しています").unbind("click");
                var coHref = $(".commu_name").attr("href");
                var m = coHref.match(/\/(co\d+)/);
                if (!m) {
                    return;
                }

                $('<iframe />')
                    .attr({
                        id: "com_archives",
                        src: "http://com.nicovideo.jp/live_archives/" + m[1],
                        width: 0,
                        height: 0,
                    })
                    .css({ border: 0 })
                    // iframeのドキュメントが更新される度に呼ばれる
                    .bind("load", function () {
                        // メッセージを送信する
                        this.contentWindow.postMessage(JSON.stringify({
                            type: "next_ts",
                            lv: window.VideoId
                        }), "*")
                    })
                    .appendTo("body");
            })
            .addToUtilityMenu();

        $(window).bind("message", function (ev) {
            var o = ev.originalEvent;
            // 放送アーカイブページからのメッセージを受け取る
            if (o.origin.indexOf("http://com.nicovideo.jp") != -1) {
                try {
                    var data = JSON.parse(o.data);
                    log("watchPageReceivedMessage", data);
                    // タイムシフトの捜索結果
                    if (data.type == "next_ts") {
                        var $archive = $("#com_archives"), $next = $("#next_ts");
                        // 探索終了まで取っておく
                        if (data.ts) {
                            $archive.data("ts", data.ts);
                        }
                        // 指定されたURLも捜索する必要がある
                        if (data.url) {
                            $archive[0].src = data.url;
                        }
                        // 捜索終了
                        else {
                            // タイムシフトがある
                            var ts = $archive.data("ts");
                            if (ts) {
                                $next.attr({
                                    title: ts.title + " 放送者:" + ts.user,
                                    href: "/watch/" + ts.lv + "?next_ts"
                                }).text("次のタイムシフトを見る");
                            }
                            // タイムシフトがない
                            else {
                                $next.attr("href", "javascript:void(0)").text("次のタイムシフトはありませんでした");
                            }
                            $archive.remove();
                        }
                    }
                } catch (ex) {
                    log(ex);
                }
            }
        });

        setObserve(window.location.search == "?auto");

        // 自動で次のタイムシフトを捜索する
        if (location.search == "?next_ts") {
            $("#next_ts").click();
        }

        log("loaded watch page");
    }

    function archivesPageLoaded() {
        // iframeでのみ実行する
        if (window.top == window.self) {
            return;
        }

        // コニュニティの放送アーカイブを取得する
        function getCommunityArchives() {
            var archives = [];
            var dateRegExp = /\d+/g;
            window.$$(".live_history tr").each(function (element, index) {
                if (index == 0) { return; }

                var $date = element.getElementsBySelector(".date");
                var date, m = $date[0].getText().match(dateRegExp);
                if (m) {
                    for (var i = 0; i < m.length; i++) {
                        m[i] = parseInt(m[i], 10);
                    }
                    date = new Date(m[0], m[1] - 1, m[2], m[3], m[4], 0, 0);
                }

                var $user = element.getElementsBySelector(".user .txt");
                var user = $user[0].getText();

                var $title = element.getElementsBySelector(".title .txt a");
                var lv = $title[0].href.match(/lv\d+/)[0];
                var hasTs = $title.length == 2;
                var title = $title[0].getText();

                var $desc = element.getElementsBySelector(".desc .txt");
                var desc = $desc[0].getText();

                archives.push({
                    date: date,
                    user: user,
                    lv: lv,
                    hasTs: hasTs,
                    title: title,
                    desc: desc,
                });
            });
            return archives;
        }

        window.Element.addMethods({
            getText: (function removeTags(html) {
                var removeTagsRegExp = /<\/?[^>]+>/gi;
                var trimLineRegExp = /^[\s\t]*|[\s\t]*$/gi;
                var removeSpaceRegExp = /[\t\s]+/gi;
                return function (element) {
                    return element.innerHTML
                        .replace(removeTagsRegExp, "")
                        .replace(trimLineRegExp, "")
                        .replace(removeSpaceRegExp, " ");
                }
            })()
        });

        window.Event.observe(window, "message", function (o) {
            // 放送試聴ページからのメッセージを受け取る
            if (o.origin.indexOf("http://live.nicovideo.jp") != -1) {
                try {
                    var data = JSON.parse(o.data);
                    log("archivesPageReceivedMessage", data);

                    if (data.type == "next_ts") {
                        var lv = parseInt(data.lv.substr(2), 10),
                            lastTs,
                            isStop = false;

                        // 放送一覧を捜索する
                        var archives = getCommunityArchives();
                        log(archives);
                        for (var i = 0; i < archives.length; i++) {
                            var item = archives[i];
                            // 新しい放送
                            if (parseInt(item.lv.substr(2), 10) > lv) {
                                // タイムシフトあり
                                if (item.hasTs) {
                                    lastTs = item;
                                }
                            }
                            // 古い放送、または現在の放送を見つけら探索を終了する
                            else {
                                isStop = true;
                                break;
                            }
                        }

                        var postData = {
                            type: "next_ts",
                            ts: lastTs,
                        }
                        // 次のページも捜索する
                        if (!isStop && archives.length > 0) {
                            var nextPage = window.$$("a.next")[0];
                            if (nextPage) {
                                postData.url = nextPage.href;
                            }
                        }
                        // メッセージを送信する
                        o.source.postMessage(JSON.stringify(postData), "*");
                    }
                } catch (ex) {
                    log(ex);
                }
            }
        });

        log("loaded archives page");
    }
})();