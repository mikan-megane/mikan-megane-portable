﻿// ==UserScript==
// @name       niconico Live 追い出し回避(一般)
// @namespace  mikan-megane.oidashi
// @version    0.4
// @description  ニコ生追い出し回避用(チャンネルにも対応)
// @copyright  2015 mikan-megane
// @include    https://secure.nicovideo.jp/payment/registration?sec=nicolive_oidashi*
// @include    https://secure.ch.nicovideo.jp/bylaw/*
// @include    http://ch.nicovideo.jp/*?ref=regular_live&bylaw_type=modal
// @include    http://ch.nicovideo.jp/*?ref=ppv_live1&bylaw_type=modal
// @run-at     document-start
// ==/UserScript==
window.history.back();