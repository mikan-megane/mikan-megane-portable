Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var gBundle = Components.classes["@mozilla.org/intl/stringbundle;1"].getService(Components.interfaces.nsIStringBundleService);
var _bundle = gBundle.createBundle("chrome://opendownload/locale/opendownload.properties");

// listeners are only relevant for Gecko < 26.0.
// we may want to deprecate this file some day.

function nsOpenDownloadListeners() {
}

nsOpenDownloadListeners.prototype = {
	classDescription: "OpenDownloadČ Listeners",
	classID: Components.ID("{e59364a7-d8fd-4fa3-a173-1a5fc70bc6fa}"),
	contractID: '@mozmonkey.com/opendownload;1',
	_xpcom_categories: [{ category: "profile-after-change" }],
	QueryInterface: XPCOMUtils.generateQI([Components.interfaces.nsIObserver, Components.interfaces.nsISupports, Components.interfaces.nsIOpenDownloadListeners]),

	/**
	 * Is it currently listening
	 * @type boolean
	 */
	listening : false,

	/**
	 * File download queue
	 * @type String[]
	 */
	queue : [],

	/**
	 * Start listening for downloads
	 */
	startListening : function(){
		this.listening = true;

		var appInfo = Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULAppInfo);
		var versionChecker = Components.classes["@mozilla.org/xpcom/version-comparator;1"].getService(Components.interfaces.nsIVersionComparator);
		if (versionChecker.compare(appInfo.platformVersion, "26.0") < 0 || appInfo.name == "SeaMonkey") {
			// I have no clue if this had ever worked (I never really touched it since I had
			// started working on this extension). Anyway, starting with Gecko 26 it won't anymore.
			// Time to drop the listeners at all. Because of reasons.
			var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
			observerService.addObserver(this, "dl-done", false);
			observerService.addObserver(this, "dl-cancel", false);
			observerService.addObserver(this, "dl-failed", false);
			observerService.addObserver(this, "opendownload-adddownload", false);
		}
	},

	/**
	 * Catch observer flags
	 */
	observe : function(subject, topic, data){
		// Note: Gecko 26+ works entirely differently.
		var appInfo = Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULAppInfo);
		var versionChecker = Components.classes["@mozilla.org/xpcom/version-comparator;1"].getService(Components.interfaces.nsIVersionComparator);

		var file;
		var download;

		if (subject) {
			if (versionChecker.compare(appInfo.platformVersion, "26.0") >= 0 && appInfo.name != "SeaMonkey") {
				// Gecko >= 26
				alert("Deprecated!"); // I'd assume no one will ever get this.
			}
			else {
				// Gecko < 26
				download = subject.QueryInterface(Components.interfaces.nsIDownload);
				file = download.targetFile || download.target;
			}
		}

		switch(topic) {
			case "profile-after-change":
				var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
				observerService.addObserver(this, "final-ui-startup", false);
			break;
			case "final-ui-startup":
				var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
				observerService.removeObserver(this, topic);
				this.startListening();
			break;
			case "dl-cancel":
			case "dl-failed":
				this.removeFromQueue(file.path);
			break;
			case "dl-done":
			   this.launchFile(file);
			break;
			case "opendownload-adddownload":
				var path = data;
				path = path.split("??")[0];
				this.queue[path.length] = path;
			break;
		}
	},

	/*
	 * RDF Observers (onAssert, onChange, onBeginUpdateBatch, onEndUpdateBatch, onMove, onUnassert)
	 */
	onAssert : function (dataSource , source , property , target){
	   if (property.Value.indexOf("#DownloadState") > -1 && target instanceof Components.interfaces.nsIRDFInt && !isNaN(target.Value) && parseInt(target.Value) == 1){
			var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
			file.initWithPath(source.Value);
			this.launchFile(file);
		}
	 },
	 onChange : function ( dataSource , source , property , oldTarget , newTarget ) {
	   if (property.Value.indexOf("#DownloadState") > -1 && newTarget instanceof Components.interfaces.nsIRDFInt && !isNaN(newTarget.Value) && parseInt(target.Value) == 1){
			var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
			file.initWithPath(source.Value);
			this.launchFile(file);
	   }
	 },
	 onBeginUpdateBatch : function (dataSource) { },
	 onEndUpdateBatch : function (dataSource) { },
	 onMove : function (dataSource , oldSource , newSource , property , target) { },
	 onUnassert : function (dataSource , source , property , targe) { },

	/**
	 * Launch the file
	 * @param {nsFile} file   The file to launch
	 */
	launchFile : function(file){
		// File not in queue
		if (file == null || this.inQueue(file.path) < 0)
			return;
		else
			this.removeFromQueue(file.path);

		// Execute
		if (file != null && file.exists()){
			try {
				var process = Components.classes["@mozilla.org/process/util;1"].createInstance(Components.interfaces.nsIProcess);
				process.init(file);
				process.run(false,[],0);
			}
			catch (e) {
				try {
					file.launch(); // removing this would probably break *ix support as reported for 3.4.0.
				}
				catch (err) {
					Components.utils.reportError("listeners (157)\n" + _bundle.GetStringFromName("error.unknown") + "\n" + e + "\n" + err);
				}
			}
		}
	},

	/**
	 * Remove file from download queue
	 * @param {String} path   The file path in the queue
	 */
	removeFromQueue : function(path){
		 var index = -1;
		 if((index = this.inQueue(path)) > -1){
			 this.queue = (new Array()).concat(this.queue.slice(0,index), this.queue.slice(++index));
		 }
	},

	/**
	 * Check if file path is in queue
	 * @param {String} path The file path to search for in queue
	 * @return {int} Index of file in queue
	 */
	inQueue : function(path){
		 for(var i = 0; i < this.queue.length; i++){
			 if(this.queue[i] == path){
				 return i;
			 }
		 }
		 return -1;
	}
};

var NSGetFactory = XPCOMUtils.generateNSGetFactory([nsOpenDownloadListeners]);